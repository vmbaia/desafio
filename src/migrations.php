<?php

use App\Database;

/**
 * Criar tabelas DB, indices e relacionados
 *
 * @return void
 */
function createTables()
{
    /**
     * Estrutura das tabelas
     */
    $tablesStructures = [
        "CREATE TABLE IF NOT EXISTS `produtos` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `nome` varchar(155) DEFAULT NULL,
            `sku` varchar(50) DEFAULT NULL,
            `descricao` varchar(500) DEFAULT NULL,
            `quantidade` int(11) DEFAULT NULL,
            `preco` double DEFAULT NULL,
            `categoria` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;",

        "CREATE TABLE IF NOT EXISTS `categorias` (
            `id` bigint(11) NOT NULL AUTO_INCREMENT,
            `codigo` varchar(20) DEFAULT NULL,
            `categoria` varchar(155) DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;"
    ];

    /**
     * Chaves estrangeiras
     */
    $tablesForeignKeys = [
        "ALTER TABLE `produtos` ADD FOREIGN KEY (`categoria`) REFERENCES `categorias`(`id`);"
    ];

    foreach ($tablesStructures as $tablesStructure) {
        Database::query($tablesStructure);
        Database::execute();
    }

    foreach ($tablesForeignKeys as $tablesForeignKey) {
        Database::query($tablesForeignKey);
        Database::execute();
    }

    /**
     * Evita criar tabelas existentes comentando um comando que chama esta função
     */
    $path_to_file = dirname(__DIR__) . '/src/routes.php';
    $file_contents = file_get_contents($path_to_file);
    $file_contents = str_replace("createTables();", "// createTables();", $file_contents);
    file_put_contents($path_to_file, $file_contents);
}
