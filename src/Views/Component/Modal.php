<?php

namespace Views\Component;


class Modal{


    public function componentModalProduto($id,$nome,$sku,$preco,$quantidade,$categoria){

        ?>

            <div class="modal fade" id="exampleModal<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Product <?=$nome ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <form method="POST" action="/atualizaProduto">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name:</label>
                            <input type="text" required value='<?=$nome ?>' name="nome" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">SKU:</label>
                            <input type="text" required value='<?=$sku ?>' name="sku" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Price:</label>
                            <input type="text" required value='<?=$preco ?>' name="preco" id="price" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Quantity:</label>
                            <input type="number" required value='<?=$quantidade ?>' name="quantidade" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Category:</label>
                            <select required class="form-control" name="categoria">       
                            <?php
                                foreach ($categoria as $post) {
                                    echo '<option value='.$post['id'].'>'.$post['categoria'].'</option>';
                                }
                            ?>
                            </select>
                        </div>
                        <input type="hidden" value='<?=$id ?>' name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
                </div>
                </div>
            </div>
 <?php   
    }

    public function componentModalCategoria($id,$codigo,$nome){

        ?>

        <div class="modal fade" id="exampleModal<?=$id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Category <?=$nome ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <form method="POST" action="/atualizaCategoria">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Code:</label>
                        <input type="text" required value='<?=$codigo ?>' name="codigo" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Category:</label>
                        <input type="text" required value='<?=$nome ?>' name="categoria" class="form-control" id="recipient-name">
                    </div>
                    <input type="hidden" value='<?=$id ?>' name="id" class="form-control" id="recipient-name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<?php
    }

}
?>