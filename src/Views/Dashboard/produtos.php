<?php
use App\Helper;
use Views\Component\Modal;

require_once APP_ROOT . '/src/Views/Include/header.php';

        ?>

  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="insereProdutos" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($data['produtosCategoria'] as $item){ ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['nome'] ?></span>
          </td>
        
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['sku'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">R$ <?=$item['preco'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['quantidade'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['categoria'] ?></span>
          </td>
        
          <td class="data-grid-td">
            <div class="actions">
              <a href="" data-toggle="modal" data-target="#exampleModal<?=$item['id'] ?>" ><div class="action edit"><span>Edit</span></div>
              <a href="/deletaProduto/<?=$item['id']; ?>" ><div class="action delete"><span>Delete</span></div></a>
            </div>
          </td>
        </tr>
      <?php
          $modal = new Modal;
          $modal->componentModalProduto($item['id'],$item['nome'],$item['sku'],$item['preco'],$item['quantidade'],$data['categoria']); 
        } ?>
    </table>
  </main>
  <!-- Main Content -->


<?php require_once APP_ROOT . '/src/Views/Include/footer.php'; ?>