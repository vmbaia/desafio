<?php
use App\Helper;

require_once APP_ROOT . '/src/Views/Include/header.php';

            ?>
                <main class="content">
                    <div class="header-list-page">
                        <h1 class="title">Dashboard</h1>
                    </div>
                    <div class="infor">
                    You have 4 products added on this store: <a href="/insereProdutos" class="btn-action">Add new Product</a>
                    </div>
                    <ul class="product-list">
                        <?php foreach ($data['produtos'] as $post) { ?>
                            <li>
                                <div class="product-image">
                                    <?php
                                    if (file_exists('./images/product/' . Helper::slug($post['sku'], '-', false) . '.jpg')) {
                                        ?>
                                        <img src="images/product/<?= Helper::slug($post['sku'], '-', false)?>.jpg" layout="responsive" style="object-fit:cover" width="164" height="145" title=<?= $post['nome']; ?> />
                                        <?php
                                    }else{
                                    ?>
                                        <img src="https://cofice.com.br/wp-content/uploads/2017/04/no-photo.jpg" layout="responsive" style="object-fit:cover" width="164" height="145" title=<?= $post['nome']; ?> />
                                    <?php } ?>
                                </div>
                                <div class="product-info">
                                <div class="product-name"><span><?=$post['nome']; ?></span></div>
                                <div class="product-price"><span class="special-price">9 available</span> <span>R$<?= $post['preco']; ?></span></div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </main>
<?php require_once APP_ROOT . '/src/Views/Include/footer.php'; ?>