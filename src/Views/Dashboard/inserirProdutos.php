<?php 

use App\Helper;

require_once APP_ROOT . '/src/Views/Include/header.php'; 

    ?>
  
        <main class="content">
            <h1 class="title new-item">New Product</h1>
            
            <form method="post" action="/insereProdutos" enctype="multipart/form-data" >
                <input type="hidden" name="token" value="<?= $_SESSION['token']; ?>">
                <div class="input-field">
                    <label for="sku" class="label">Product SKU</label>
                    <input type="text" required id="sku" name="sku" class="input-text" /> 
                </div>
                <div class="input-field">
                    <label for="name" class="label">Product Name</label>
                    <input type="text" required  id="name" name="nome" class="input-text" /> 
                </div>
                <div class="input-field">
                    <label for="price" class="label">Price</label>
                    <input type="real" required id="price" name="preco" class="input-text" /> 
                </div>
                <div class="input-field">
                    <label for="quantity" class="label">Quantity</label>
                    <input type="number" required id="quantity" name="quantidade" class="input-text" /> 
                </div>
                <div class="input-field">
                    <label for="image" class="label">Imagem</label>
                    <input type="file" name="imagem" id="image" class="input-text" /> 
                </div>
                <div class="input-field">
                    <label for="category" class="label">Categories</label>
                    <select multiple required id="category" name="categoria" class="input-text">
                        <?php
                            foreach ($data['categoria'] as $post) {
                                echo '<option value='.$post['id'].'>'.$post['categoria'].'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="input-field">
                    <label for="description" class="label">Description</label>
                    <textarea required id="description" name="descricao" class="input-text"></textarea>
                </div>
                <div class="actions-form">
                    <a href="/produtos" class="action back">Back</a>
                    <button type="submit" class="btn-submit btn-action form-button" >Save Product</button>
                </div>
            </form>
        </main>

<?php require_once APP_ROOT . '/src/Views/Include/footer.php'; ?>