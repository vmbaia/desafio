<?php
use App\Helper;
use Views\Component\Modal;

require_once APP_ROOT . '/src/Views/Include/header.php';

        ?>

  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="/insereCategorias" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($data['categoria'] as $item){ ?>
        <tr class="data-row">
            <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['categoria'] ?></span>
            </td>
        
            <td class="data-grid-td">
            <span class="data-grid-cell-content"><?=$item['codigo'] ?></span>
            </td>
        
            <td class="data-grid-td">
            <div class="actions">
                <a href="" data-toggle="modal" data-target="#exampleModal<?=$item['id'] ?>"><div class="action edit"><span>Edit</span></div></a>
                <a href="/deletaCategoria/<?=$item['id'] ?>"><div class="action delete"><span>Delete</span></div></a>
            </div>
            </td>
        </tr>
      <?php 
        $modal = new Modal;
        $modal->componentModalCategoria($item['id'],$item['codigo'],$item['categoria']);
    
    } ?>
    </table>
  </main>
  <!-- Main Content -->

<?php require_once APP_ROOT . '/src/Views/Include/footer.php'; ?>