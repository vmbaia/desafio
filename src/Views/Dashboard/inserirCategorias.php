<?php 

use App\Helper;

require_once APP_ROOT . '/src/Views/Include/header.php'; 

    ?>
  
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    
    <form method="POST" action="/insereCategorias">
      <input type="hidden" name="token" value="<?= $_SESSION['token']; ?>">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="categoria_nome" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="categoria_codigo" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="/categoria" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

<?php require_once APP_ROOT . '/src/Views/Include/footer.php'; ?>