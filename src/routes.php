<?php

use App\Router;

/**
 * Rotas web
 */

 //GET
    
    //Produtos
    Router::get('/', 'Controllers\ProdutosController@index');
    Router::get('/produtos', 'Controllers\ProdutosController@ProdutosCategoria');
    Router::get('/insereProdutos', 'Controllers\ProdutosController@viewProduto');
    Router::get('/deletaProduto/(:any)', 'Controllers\ProdutosController@deletaProduto');

    //Categorias
    Router::get('/categoria', 'Controllers\CategoriasController@Categorias');
    Router::get('/insereCategorias', 'Controllers\CategoriasController@createCategoria');
    Router::get('/deletaCategoria/(:any)', 'Controllers\CategoriasController@deletaCategoria');

//POST

    //Produtos
    Router::post('/insereProdutos', 'Controllers\ProdutosController@insereProduto');
    Router::post('/atualizaProduto', 'Controllers\ProdutosController@atualizaProduto');

    //Categorias
    Router::post('/insereCategorias', 'Controllers\CategoriasController@insereCategoria');
    Router::post('/atualizaCategoria', 'Controllers\CategoriasController@atualizaCategoria');


/**
 *  Se alguma rota não for definida corretamente
 */
Router::error(function () {
    die('404 Page not found');
});

/**
 * Remova o comentário desta função para migrar tabelas
 * Será comentado automaticamente novamente
 */
   // createTables();

Router::dispatch();
