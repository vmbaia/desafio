<?php

namespace Models;

use App\Database;
use App\Helper;

class Produtos
{
    /**
     * Seleciona Produtos
     *
     * @param integer $count
     * @return array
     */
    public static function index($count = 0)
    {
        if ($count === 0) {
            Database::query("SELECT * FROM produtos ORDER BY id DESC");
        } else {
            Database::query("SELECT * FROM produtos ORDER BY id DESC LIMIT :count");
            Database::bind(':count', $count);
        }

        return Database::fetchAll();
    }

     /**
     * Seleciona produtos de acordo com sua categorias
     *
     * @param integer $count
     * @return array
     */
    public static function ProdutoCategoria($count = 0)
    {
        if ($count === 0) {
            Database::query("SELECT produtos.id as id,produtos.nome as nome,produtos.sku as sku,produtos.preco as preco,produtos.quantidade as quantidade,categorias.categoria as categoria FROM produtos inner join categorias on produtos.categoria = categorias.id ORDER BY produtos.id DESC");
        } else {
            Database::query("SELECT produtos.id as id,produtos.nome as nome,produtos.sku as sku,produtos.preco as preco,produtos.quantidade as quantidade,categorias.categoria as categoria FROM produtos inner join categorias on produtos.categoria = categorias.id ORDER BY produtos.id DESC LIMIT :count");
            Database::bind(':count', $count);
        }

        return Database::fetchAll();
    }

    /**
     * inserção
     *
     * @param object $request
     * @return bool
     */
    public static function insere($request)
    {

        Database::query("INSERT INTO produtos (
            nome,
            sku,
            descricao,
            quantidade,
            preco,
            categoria
        ) VALUES (:nome, :sku, :descricao, :quantidade, :preco, :categoria)");
        Database::bind(':nome', $request->nome);
        Database::bind(':sku', $request->sku);
        Database::bind(':descricao', $request->descricao);
        Database::bind(':quantidade', $request->quantidade);
        Database::bind(':preco', $request->preco);
        Database::bind(':categoria', $request->categoria);

        if (Database::execute()) return true;
        return false;
    }

    /**
     * Atualização
     *
     * @param object $request
     * @return bool
     */
    public static function atualiza($request)
    {
        Database::query("UPDATE produtos SET
            nome = :nome,
            sku = :sku,
            preco = :preco,
            quantidade = :quantidade,
            categoria = :categoria
        WHERE id = :id");
        Database::bind(':nome', $request->nome);
        Database::bind(':sku', $request->sku);
        Database::bind(':preco', $request->preco);
        Database::bind(':quantidade', $request->quantidade);
        Database::bind(':categoria', $request->categoria);
        Database::bind(':id', $request->id);

        if (Database::execute()) return true;
        return false;
    }

    /**
     * Deleção
     *
     * @param string $slug
     * @return bool
     */
    public static function deleta($slug)
    {
        Database::query("DELETE FROM produtos WHERE id = :slug");
        Database::bind(':slug', $slug);

        if (Database::execute()) return true;
        return false;
    }

    public static function deletaProdutoDaCategoria($slug)
    {
        Database::query("DELETE FROM produtos WHERE categoria = :slug");
        Database::bind(':slug', $slug);

        if (Database::execute()) return true;
        return false;
    }
}
