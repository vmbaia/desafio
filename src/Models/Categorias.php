<?php

namespace Models;

use App\Database;
use App\Helper;

class Categorias
{
    
    /**
     * select de categorias
     *
     * @param integer $count
     * @return array
     */
    public static function categoria()
    {
        Database::query("SELECT * FROM categorias ORDER BY id DESC");
        return Database::fetchAll();
    }

     /**
     * inserção
     *
     * @param object $request
     * @return bool
     */
    public static function insere($request)
    {

        Database::query("INSERT INTO categorias (
            codigo,
            categoria
        ) VALUES (:codigo, :categoria)");
        Database::bind(':codigo', $request->categoria_codigo);
        Database::bind(':categoria', $request->categoria_nome);

        if (Database::execute()) return true;
        return false;
    }

    /**
     * Atualização
     *
     * @param object $request
     * @return bool
     */
    public static function atualiza($request)
    {
        Database::query("UPDATE categorias SET
            codigo = :codigo,
            categoria = :categoria
        WHERE id = :id");
        Database::bind(':codigo', $request->codigo);
        Database::bind(':categoria', $request->categoria);
        Database::bind(':id', $request->id);

        if (Database::execute()) return true;
        return false;
    }


     /**
     * Deleção
     *
     * @param string $slug
     * @return bool
     */
    public static function deleta($slug)
    {
        Database::query("DELETE FROM categorias WHERE id = :slug");
        Database::bind(':slug', $slug);

        if (Database::execute()) return true;
        return false;
    }

}
