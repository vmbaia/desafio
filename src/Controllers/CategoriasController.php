<?php

namespace Controllers;

use App\Database;
use App\HandleForm;
use App\Helper;
use App\XmlGenerator;
use Models\Categorias;
use Models\Produtos;

class CategoriasController
{

    // View formulario de inserção de categorias
    
    public function createCategoria()
    {

        Helper::render(
            'Dashboard/inserirCategorias',
            [
                'page_title' => 'Adicionar Categoria',
                'page_subtitle' => 'Adicionar uma nova Categoria'
            ]
        );
    }

     /**
     * View tabela de categorias
     *
     * @return void
     */
    public function Categorias()
    {
        Helper::render(
            'Dashboard/categorias',
            [
                'page_title' => 'Categoria',
                'categoria' => Categorias::categoria()
            ]
        );
    }

     /**
     * Insere
     *
     * @return void
     */
    public function insereCategoria()
    {
        $request = json_decode(json_encode($_POST));

        if (Helper::csrf($request->token) && Categorias::insere($request)) {
            unset($_POST);
            header('Location: /categoria');
        } else {
            header('Location: /insereCategorias');
        }
    }

     /**
     * Atualiza
     *
     * @return void
     */
    public function atualizaCategoria()
    {
        $request = json_decode(json_encode($_POST));
        Categorias::atualiza($request); 
        header('Location: /categoria');  
    }


    /**
     * Deleta
     *
     * @param string $slug
     * @return void
     */
    public function deletaCategoria($slug)
    {
        Categorias::deleta($slug);
        Produtos::deletaProdutoDaCategoria($slug);
        header('Location: /categoria');
    }


}
