<?php

namespace Controllers;

use App\Database;
use App\HandleForm;
use App\Helper;
use App\XmlGenerator;
use Models\Produtos;
use Models\Categorias;

class ProdutosController
{
     /**
     * view indice da aplicação
     *
     * @return void
     */
    public function index()
    {
        Helper::render(
            'Dashboard/index',
            [
                'page_title' => 'Dashboard',
                'produtos' => Produtos::index()
            ]
        );
    }

     /**
     * View tabela de edição de produtos
     *
     * @return void
     */
    public function ProdutosCategoria()
    {
        Helper::render(
            'Dashboard/produtos',
            [
                'page_title' => 'Produtos',
                'produtosCategoria' => Produtos::ProdutoCategoria(),
                'categoria' => Categorias::categoria()
            ]
        );
    }

     /**
     * View formulario de inserção de produtos
     *
     * @return void
     */
    public function viewProduto()
    {

        Helper::render(
            'Dashboard/inserirProdutos',
            [
                'page_title' => 'Adicionar Produto',
                'categoria' => Categorias::categoria()
            ]
        );
    }

     /**
     * Insere
     *
     * @return void
     */
    public function insereProduto()
    {

        $request = json_decode(json_encode($_POST));

        if (Helper::csrf($request->token) && Produtos::insere($request)) {
            if (isset($_FILES['imagem']['type'])) {
                HandleForm::upload($_FILES['imagem'], ['jpeg', 'jpg','png'], 5000000, '../public/images/product/',
                85, Helper::slug($request->sku, '-', false));
            }
            unset($_POST);
            header('Location: /produtos');
        } else {
            header('Location: /insereProdutos');
        }
    }

     /**
     * Atualiza
     *
     * @return void
     */
    public function atualizaProduto()
    {
        $request = json_decode(json_encode($_POST));
        Produtos::atualiza($request); 
        header('Location: /produtos');  
    }
    
     /**
     * Deleta
     *
     * @param string $slug
     * @return void
     */
    public function deletaProduto($slug)
    {
        Produtos::deleta($slug);
        header('Location: /produtos');
    }
}
