### Desafio Webjump

#### Features:
- **public:**
contém o arquivo index.php, para iniciar o aplicativo e configura o carregamento automático. Diferentes configurações de servidor adicionadas a este diretório também. Por fim, você pode encontrar o gerador de Sitemap que é executado após a criação ou atualização de uma postagem.
- **public/css** & **public/js:**
arquivos voltados para o front da aplicação.
- **public/images** & **public/images/product** & **public/images/product-page:**
voltado para arquivos de media da aplicação.
- **src:**
contém migrações para um banco de dados e rotas.
- **src/App:**
contém todas as classes usadas em códigos como PDO, HandleForm, Router e ...
- **src/Controllers:**
controladores relacionados com suas rotas separados para web e aplicativo.
- **src/Models:**
modelos relacionados a consultas e requisitos de banco de dados dos controladores.
- **src/Views:**
arquivos PHP simples para mostrar dados no Frontend com arquivos de inclusão reutilizáveis.

#### Funções úteis:
- **HandleForm::upload(...)**
carregar arquivo, redimensionar imagem e adicionar marca d'água
- **Helper::csrf(...)**
verifique o token de falsificação de solicitação entre sites
- **Helper::slug(...)**
slugify string para tornar o URL amigável

#### Rodar aplicação web:
- use o gerenciador de dependencias `composer` ultilizando o comando `composer dump-autoload` 
- crie o banco de dados MySQL e mude as credenciais no `env.php`
- descomente `// createTables();` em `src/routes` _(ele criará as tabelas relacionadas com as migrações,e comentará o `createTables();` automaticamente.)_
- Use a linha de comando para exibi-lo no localhost: 8080: `php -S localhost:8080 -t public/`.

------------